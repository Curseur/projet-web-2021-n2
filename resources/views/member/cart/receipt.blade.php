<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Facture</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="information">
                <td colspan="3">
                    <h5><strong>EgameCenter</strong></h5>
                </td>
            </tr>
            <tr class="heading">
                <td>Game</td>
                <td>Game Code</td>
                <td>Price</td>
            </tr>
            @foreach (Cart::content() as $game)
                <tr class="item">
                    <td>{{ $game->model->name}}</td>
                    <td>{{ $game->model->code }}</td>
                    <td>{{ $game->model->price }} €</td>
                </tr>
            @endforeach
            <tr class="total">
                <td><strong>Total: {{ Cart::subtotal() }} €</strong></td>
            </tr>
        </table>
    </div>
</body>
</html>
<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $user = auth()->user();

        return view('member.profile.index')
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('member.profile.edit')
            ->with(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user = auth()->user();

        $name = $request->input('name');
        $email = $request->input('email');

        $user = User::find($user->id);
        $user->name = $name;
        $user->email = $email;

        if($user->save())
        {
            $request->session()->flash('success', $user->name . ' has been modified successfully !');
        }else
        {
            $request->session()->flash('error', 'The User has not been modified !');
        }

        return redirect()->route('member.profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addMoney(User $user)
    {
        return view('member.profile.addMoney')
            ->with(['user' => $user]);
    }

    public function updateMoney(Request $request, User $user)
    {
        $user = auth()->user();

        $moneys = $request->input('moneys');

        $user = User::find($user->id);
        $user->moneys += $moneys;

        if($user->save())
        {
            $request->session()->flash('success', 'Your Moneys has been added successfully !');
        }else
        {
            $request->session()->flash('error', 'Your Moneys has not been added !');
        }

        return redirect()->route('member.profile.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        //if($search != '')
        //{
        //    $games = Game::where('name', 'like', '%' . $search . '%')
        //        ->get();
        //}else
        //{
            $games = Game::paginate(5);
        //}

        return view('home')->with('games', $games);
    }
}

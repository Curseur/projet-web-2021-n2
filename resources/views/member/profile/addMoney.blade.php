@extends('layouts.app')

@section('content')
<div class="container">
    <form action=" {{ route('member.profile.updateMoney') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="moneys" class="form-label">Moneys</label>
            <input type="number" name="moneys" class="form-control" value="{{ auth()->user()->moneys }}">
        </div>
        
        <button type="submit" class="btn btn-primary">Add Money</button>
    </form>
</div>
@endsection
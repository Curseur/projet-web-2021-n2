<?php

namespace App\Http\Controllers\Member;

use App\Game;
use App\Cart;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use PDF;

class PdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF(Game $game, User $user)
    {
        $user = auth()->user();
        $currentUser = User::find(auth()->user()->id);

        $total = Cart::Subtotal();

        $moneys = $currentUser->moneys;

        if($moneys - $total >= 0)
        {
            $pdf = PDF::loadView('member/cart/receipt');
            $pdf->save('receipt.pdf');

            $data["email"] = $user->email;
            $data["title"] = "EgameCenter";

            $files = [
                public_path('receipt.pdf'),
            ];

            Mail::send('member/cart/receipt', $data, function ($message) use ($data, $files)
            {
                $message->to($data["email"], $data["email"])
                    ->subject($data["title"]);

                foreach($files as $file)
                {
                    $message->attach($file);
                }
            });

            $newMoneys = $moneys - $total;
            $currentUser->moneys = $newMoneys;
            $currentUser->save();

            Cart::destroy();
            return redirect()
                ->route('member.cart.index')->with('game', $game)
                ->with('success', 'Your purchase went well ! You will receive your Invoice and the activation code for your Game !');
        }else
        {
            return redirect()->route('member.cart.index')->with('error', 'Insufficient money !');
        }
    }
}

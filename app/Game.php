<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function notices()
    {
        return $this->hasMany(Notice::class, 'id_game');
    }
}

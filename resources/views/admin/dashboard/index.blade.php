@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Dashboard Admin
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Number of members : <strong>{{ $userCount }}</strong></li>
                        <li class="list-group-item">Number of games : <strong>{{ $gameCount }}</strong></li>
                        <li class="list-group-item">Number of sales : </li>
                        <li class="list-group-item">Number of new sales over the last 7 days : </li>
                        <li class="list-group-item">Total site revenues : €</li>
                        <li class="list-group-item">Total site revenues over the last 7 days : €</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
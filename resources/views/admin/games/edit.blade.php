@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{ route('admin.games.update', $game) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="name" class="form-label">Name Game</label>
            <input type="text" name="name" class="form-control" value="{{ $game->name }}">
        </div>

        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <input type="text" name="description" class="form-control" value="{{ $game->description }}" >
        </div>

        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="number" name="price" class="form-control" value="{{ $game->price }}">
        </div>

        <div class="mb-3">
            <label for="platform" class="form-label">Platform</label>
            <select class="form-select" name="platform">
                <option selected>{{ $game->platform }}</option>
                <option value="PC">PC</option>
                <option value="XBOXONE">XBOX</option>
                <option value="XBOXX">XBOX Series X</option>
                <option value="PS4">PS4</option>
                <option value="PS5">PS5</option>
            </select>
        </div>

        <div class="mb-3">
            <label for="quantity" class="form-label">Quantity</label>
            <input type="number" name="quantity" class="form-control" value="{{ $game->quantity }}">
        </div>

        <div class="mb-3">
            <label for="code" class="form-label">Game Code</label>
            <input type="text" name="code" class="form-control" value="{{ $game->code }}">
        </div>

        <div class="mb-3">
            <label for="gameScreen" class="form-label">Image of Game</label>
            <input type="file" name="gameScreen" class="form-control" id="gameScreen">
        </div>

        <button type="submit" class="btn btn-primary">Edit Game</button>
    </form>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="my-4">EgameCenter</h1>
            <div class="list-group">
            </div>
        </div>

        <div class="col-lg-9">
            <div class="card mt-4">
                <img style="height: 400px;" class="card-img-top img-fluid" src="{{ asset('storage/gameScreen/' . $game->gameScreen) }}" alt="">
                <div class="card-body">
                    <h3 class="card-title">{{ $game->name }}</h3>
                    <p class="card-text"><strong>Description : </strong> {{ $game->description }}</p>
                    <h4>{{ $game->price }} €</h4>
                    @if (Auth::check())
                        <form action="{{ route('member.cart.store') }}" method="POST">
                            @csrf
                                <input type="hidden" name="game_id" value="{{ $game->id }}">
                                <button type="submit" class="btn btn-success">Add in your Cart</button>
                        </form>
                    @else
                        <a href="{{ route('login') }}">
                            <button type="button" class="btn btn-success">Add in your Cart</button>
                        </a>
                    @endif
                </div>
            </div>

            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    Notices of Game
                </div>

                <div class="card-body">
                    @foreach($game->notices as $notice)
                        <p>{{ $notice->description }}</p>
                        <h3>{{ $notice->note }}/10</h3>
                        <small class="text-muted">Notice by {{ $notice->auteur }} on {{ $notice->created_at }}
                            @if (Auth::check())
                                @if($notice->auteur === auth()->user()->name)
                                    <div style="display: flex;">
                                        <a class="btn" href="{{ route('member.notice.edit',['id_game'=>$game->id, $notice ]) }}"><i class="fa fa-edit"></i></a>
                                        <form action=" {{ route('member.notice.destroy', $notice) }}" method="POST" style="margin-left: 10px;">
                                            @csrf
                                            @method("DELETE")
                                            <button class="btn"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                @endif
                            @endif
                        </small>
                        <hr>
                    @endforeach
                    <a href=" {{ route('member.notice.create', ['id_game'=>$game->id]) }} " class="btn btn-success">Add Notice</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="my-4">EgameCenter</h1>
            <div class="list-group">
                <a href="#" class="list-group-item active">Category #1</a>
                <a href="#" class="list-group-item">Category #2</a>
                <a href="#" class="list-group-item">Category #3</a>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="card mt-4">
                <img style="height: 400px;" class="card-img-top img-fluid" src="{{ asset('storage/gameScreen/' . $game->gameScreen) }}" alt="">
                <div class="card-body">
                    <h3 class="card-title">{{ $game->name }}</h3>
                    <p class="card-text">{{ $game->description }}</p>
                    <h4>{{ $game->price }} €</h4>
                    <br><br>
                    @if($game->quantity === 0)
                        <button type="button" class="btn btn-success disabled">Buy</button>
                    @else()
                        <button type="button" class="btn btn-success">Buy</button>
                    @endif
                </div>
            </div>

            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    Notices
                </div>
                <div class="card-body">
                    @foreach($game->notices as $notice)
                        <p>{{ $notice->description }}</p>
                        <h3>{{ $notice->note }}</h3>
                        <small class="text-muted">Posted by {{ $notice->auteur }} on {{ $notice->created_at }} 
                        @if($notice->auteur === auth()->user()->name)
                            <a href="{{ route('member.notice.edit',["id_game"=>$game->id, $notice ]) }}"><img src=""/>Edit Notice</a> 
                            <form action="{{ route('member.notice.destroy', $notice) }}" method="POST" style="margin-left: 10px;">
                                @csrf
                                @method("DELETE")
                                <button class="btn btn-light"><img src=""/>Delete Notice</button>  
                            </form>
                        @endif
                        </small>
                        <hr>
                    @endforeach
                <a href=" {{ route("member.notice.create", ["id_game"=>$game->id]) }} " class="btn btn-success">Add Notice</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
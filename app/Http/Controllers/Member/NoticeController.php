<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Notice;
use App\Game;
use App\User;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Game $game)
    {
        $id_game = $request->input('id_game');
        return view('member.notice.create', [
            'id_game' => $id_game
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $note = $request->input('note');
        $description = $request->input('description');
        $game_id = $request->input('id_game');
        $author = $user = auth()->user()->name;

        $notice = new Notice;
        $notice->note = $note;
        $notice->description = $description;
        $notice->id_game = $game_id;
        $notice->author = $author;
        $notice->save();

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice, Game $game, Request $request)
    {
        $id_game = $request->input('id_game');

        return view('member.notice.edit', ['id_game' => $id_game, 'notice' => $notice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        $note = $request->input('note');
        $description = $request->input('description');

        $notice = Notice::find($notice->id);
        $notice->note = $note;
        $notice->description = $description;
        $notice->save();

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice, Game $game)
    {
        $notice->delete();

        return redirect()->route('home');
    }
}

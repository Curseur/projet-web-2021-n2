<?php

namespace App\Http\Controllers\Member;

use App\Cart;
use App\Game;
use App\User;
use App\Http\Controllers\Controller;
use App\Mail\CheckUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Game $game)
    {
        return view('member.cart.index')
            ->with('game', $game);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $double = Cart::search(function ($cartItem, $rowId) use ($request)
        {
            return $cartItem->id == $request->game_id;
        });

        if($double->isNoEmpty())
        {
            return redirect()->route('home')
                ->with('success', 'The Game has already been added');
        }

        $game = Game::find($request->game_id);

        Cart::add($game->id, $game->name, 1, $game->price)
            ->associate('App\Game');

        return redirect()->route('home')->with('success', 'The Game has been added in your Cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
        Cart::remove($rowId);

        return back()->with('success', 'The Game has been deleted successfully !');
    }
}

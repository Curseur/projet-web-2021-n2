@extends('layouts.app')

@section('content2')
@endsection

@section('content')
<div class="container">
    <div class="row">
        @foreach($games as $game)
            <div class="col-lg-4 col-sm-6 portfolio-item">
                <div class="card h-100" style="width: 18rem;">
                    <a href="{{route('member.games.show', $game->id)}}"><img style="height: 150px;" class="card-img-top" src="{{ asset('storage/gameScreen/' . $game->gameScreen) }}" alt=""></a>
                    <div class="card-body">
                        <h4 class="card-title">
                            <h5 class="card-title" href="#"><strong>Game Name :</strong> {{ $game->name }}</h5>
                        </h4>
                        <p class="card-text"><strong>Price :</strong> {{ $game->price }} €</p>
                        <p class="card-text"><strong>Platform :</strong> {{ $game->platform }}</p>
                        @if (Auth::check())
                            <form action="{{ route('member.cart.store') }}" method="POST">
                                @csrf
                                <input type="hidden" name="game_id" value="{{ $game->id }}">
                                <button type="submit" class="btn btn-success">Add in your Cart</button>
                            </form>
                        @else
                            <a href="{{ route('login') }}">
                                <button type="button" class="btn btn-success">Add in your Cart</button>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
        {{ $games->links() }}
    </div>
</div>
@endsection
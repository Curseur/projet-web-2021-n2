<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\Notice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Facade\Ignition\Exceptions\ViewException;

class GamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search') ?? '';

        if($search != '')
        {
            $games = Game::where('name', 'like', '%' . $search . '%')
                ->get();
        }else
        {
            $games = Game::all();
        }

        return view('admin.games.index')
            ->with('games', $games);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.games.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('gameScreen'))
        {
            $fileNameWithExt = $request->file('gameScreen')->getClientOriginalName();
        }

        if($request->hasFile('gameScreen'))
        {
            $fileNameWithExt = $request->file('gameScreen')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('gameScreen')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'.'.$extension;
            $path = $request->file('gameScreen')->storeAs('public/gameScreen', $fileNameToStore);
        }else
        {
            $fileNameToStore = 'noscreen.jpg';
        }

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $platform = $request->input('platform');
        $quantity = $request->input('quantity');
        $code = $request->input('code');

        $game = new Game();
        $game->name = $name;
        $game->description = $description;
        $game->price = $price;
        $game->platform = $platform;
        $game->quantity = $quantity;
        $game->code = $code;
        $game->gameScreen = $fileNameToStore;
        $game->save();

        return redirect()->route('admin.games.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game, Notice $notice)
    {
        return view('admin.games.show', ['game' => $game, 'notice' => $notice]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        return view('admin.games.edit')
            ->with('game', $game);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        if($request->hasFile('gameScreen'))
        {
            $fileNameWithExt = $request->file('gameScreen')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('gameScreen')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'.'.$extension;
            $path = $request->file('gameScreen')->storeAs('public/gameScreen', $fileNameToStore);
        }else
        {
            $fileNameToStore = 'noscreen.jpg';
        }

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $platform = $request->input('platform');
        $quantity = $request->input('quantity');
        $code = $request->input('code');

        $game = Game::find($game->id);
        $game->name = $name;
        $game->description = $description;
        $game->price = $price;
        $game->platform = $platform;
        $game->quantity = $quantity;
        $game->code = $code;
        $game->gameScreen = $fileNameToStore;

        if($game->save())
        {
            $request->session()->flash('success', $game->name . ' has been modified successfully !');
        }else
        {
            $request->session()->flash('error', 'Error ! The Game has not been modified !');
        }

        return redirect()->route('admin.games.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Game $game)
    {
        $game->delete();

        if($game->delete())
        {
            $request->session()->flash('error', 'Error ! The Game has not been deleted !');
        }else
        {
            $request->session()->flash('success', $game->name . ' has been deleted !');
        }

        return redirect()->route('admin.games.index');
    }
}
